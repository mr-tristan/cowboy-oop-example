package org.apcs;

/*
    IMPORTANT: THIS IS THE COMPLETED COWBOY OBJECT THAT STUDENTS WILL NEED TO IMPLEMENT!
    Have students start from scratch with an empty class.
 */
public class Cowboy {
    private int x;
    private int y;
    private String name;

    // Getters
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getName() {
        return name;
    }

    // Setters
    public void setX(int newX) {
        x = newX;
    }

    public void setY(int newY) {
        y = newY;
    }

    public void setName(String newName) {
        name = newName;
    }
}
