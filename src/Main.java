/*
    Cowboy Object-Oriented Programming (OOP) Example

    Description: Students will implement the `Cowboy` object and change its position on the game board using
                 `getters` and `setters`. Students will create a small game where they move a `Cowboy` through
                  a maze of trees to reach a cow at the end of the path.

    SWBAT:
        - Understand the difference between private and public.
        - Understand the difference between a `getter` and a `setter`.
        - Write `getter` and `setter` methods.
        - Design and implement an object in Java.
*/

import java.util.Scanner;
import org.apcs.Cowboy;

class Main {
    public static String[][] map = {
            { "🌳", "🌳", "🌳", "🌳", "🌳" },
            { "🌳", "🐄", "🌳", "🌳", "🌳" },
            { "🌳", "  ", "🌳", "🌳", "🌳" },
            { "🌳", "  ", "  ", "  ", "🌳" },
            { "🌳", "🌳", "🌳", "  ", "🌳" },
            { "🌳", "🌳", "🌳", "  ", "🌳" },
            { "🌳", "🌳", "🌳", "  ", "🌳" },
    };


    public static void drawMap() {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                System.out.print(map[i][j]);
            }
            System.out.println();
        }
    }

    public static String[][] updateCowboyPositionOnMap(Cowboy cowboy, String direction) {
        String[][] ret = map;
        try {
            ret[cowboy.getX()][cowboy.getY()] = "🤠";

            switch(direction) {
                case "north":
                    ret[cowboy.getX() + 1][cowboy.getY()] = "  ";
                    break;
                case "south":
                    ret[cowboy.getX() - 1][cowboy.getY()] = "  ";
                    break;
                case "east":
                    ret[cowboy.getX()][cowboy.getY() - 1] = "  ";
                    break;
                case "west":
                    ret[cowboy.getX()][cowboy.getY() + 1] = "  ";
                    break;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return ret;
        }

        return ret;
    }

    public static boolean canMoveInDirection(String direction, Cowboy cowboy) {
        switch (direction) {
            case "north":
                try {
                    if (map[cowboy.getX() - 1][cowboy.getY()].equals("🌳"))
                        return false;
                    else
                        return true;
                } catch (ArrayIndexOutOfBoundsException e) {
                    return false;
                }
            case "south":
                try {
                    if (map[cowboy.getX() + 1][cowboy.getY()].equals("🌳"))
                        return false;
                    else
                        return true;
                } catch (ArrayIndexOutOfBoundsException e) {
                    return false;
                }
            case "east":
                try {
                    if (map[cowboy.getX()][cowboy.getY() + 1].equals("🌳"))
                        return false;
                    else
                        return true;
                } catch (ArrayIndexOutOfBoundsException e) {
                    return false;
                }
            case "west":
                try {
                    if (map[cowboy.getX()][cowboy.getY() - 1].equals("🌳"))
                        return false;
                    else
                        return true;
                } catch (ArrayIndexOutOfBoundsException e) {
                    return false;
                }
            default:
                return false;
        }
    }

    public static void main(String[] args) {
        Cowboy cowboy = new Cowboy();

        // IMPORTANT: Don't give students this line of code!
        // Students will have to figure this part out after implementing their `Cowboy`.
        cowboy.setX(6);
        // IMPORTANT: Don't give students this line of code!
        // Students will have to figure this part out after implementing their `Cowboy`.
        cowboy.setY(3);

        Scanner scan = new Scanner(System.in);

        System.out.println("Welcome pardner! What's your name?");

        // IMPORTANT: Don't give students this line of code!
        // Students will have to figure this part out after implementing their `Cowboy`.
        cowboy.setName(scan.nextLine());

        System.out.println("Nice ta meetcha Cowboy " + cowboy.getName() + "!");
        System.out.println("Can you help me wrangle my lost cow? I lose 'em in the woods!");
        System.out.println("I'd be much obliged!");

        String cmd = scan.nextLine();
        while (!cmd.equals("exit")) {

            if (cmd.equals("north") && canMoveInDirection(cmd, cowboy)) {
                // IMPORTANT: Don't give students this line of code!
                // Students will have to figure this part out after implementing their `Cowboy`.
                cowboy.setX(cowboy.getX() - 1);
            }

            if (cmd.equals("south") && canMoveInDirection(cmd, cowboy)) {
                // IMPORTANT: Don't give students this line of code!
                // Students will have to figure this part out after implementing their `Cowboy`.
                cowboy.setX(cowboy.getX() + 1);
            }

            if (cmd.equals("east") && canMoveInDirection(cmd, cowboy)) {
                // IMPORTANT: Don't give students this line of code!
                // Students will have to figure this part out after implementing their `Cowboy`.
                cowboy.setY(cowboy.getY() + 1);
            }

            if (cmd.equals("west") && canMoveInDirection(cmd, cowboy)) {
                // IMPORTANT: Don't give students this line of code!
                // Students will have to figure this part out after implementing their `Cowboy`.
                cowboy.setY(cowboy.getY() - 1);
            }

            map = updateCowboyPositionOnMap(cowboy, cmd);
            drawMap();
            cmd = scan.nextLine();
        }
    }
}
